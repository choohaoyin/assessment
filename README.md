# Internship Interview Questions

1. In Java, the maximum size of an Array needs to be set upon initialization. Supposedly, we want something like an Array that is dynamic, such that we can add more items to it over time. Suggest how we can accomplish that (other than using ArrayList)?
    
    There are some ways to achieve dynamic array without using Arraylist, but the performance will definitely not as efficient as those pre-defined collection that available in a programming language library as those are optimized by many programmers.

    The first way to achieve dynamic array is that we can first initialise an array with a size that larger than the number of data that are known to be required to store into the array. For instance, if we planning to store 5 data into an array, we can initialise an array with the size of 10, so in future we still have 5 reserved empty space for storing additional data. However, this approach may eventually fill up all the reserved empty space in the array. Therefore, this implementation needs to check the number of empty reserved space in the array everytime a data is inserted into the array, if the number of empty reserved space is running low, a new array with a size that larger than the current array need to be initialised and all data of the current array need to copy into the new array, so that there are more reserved empty space for future data. This approach may seem to solve the problem of the restriction of an array, however this approach have some disadvantages such as too much empty space is reserved for future data, if there is no data will be added into the array, the reserved space will be wasted and the larger the margin of reserved allocation, the more space will be wasted.

    The second way to achieve dynamic array can address the overhead problem of the first solution. We can implement the array like a linkedlist, meaning that an array with two space will be created everytime when a new data needs to be inserted to the array. The two space of the array will be used to store the data and the pointer to next node. The first array to be initialised will act as the root node, everytime when a new array is created to store a new data, the pointer of the newly created array will be stored into the previous node meaning that each node will have the pointer of their subsequent node. So, if we want to dynamically adding data into the array, we just have to locate the last node, create a new two space array, store the pointer to the last node, and insert the data to the newly created array. Insertion and deletion of this implementation will be fast as only the pointer required to be modified if any node is inserted or deleted in between some node. However, this approach has its disadvantage where transversal in the array may be not efficient. For instance, if we want to retrieve data from the last node, we have to transverse from the root node all the way to the last node in order to retrieve the last data as there is no hash table for direct access to the node.
---
2. Explain this block of code in Big-O notation.
    ```
    void sampleCode(int arr[], int size)
    {
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                printf("%d = %d\n", arr[i], arr[j]);
            }
         }
    }
    ```
    
    This block of code can be represented with Big-O notation of O(N^2) which also known as quadratic/polynomial computational complexity which means the performance of this block of code is directly proportional to the square of the input size. In the block of code showing a function that receives a 2-dimensional (2-d) array and the size of the array as input. The function have a nested loop to retrieve and print out every single element within the 2-d array that likely to be have 2 equal dimension. 
    
    To retrieve and print out all element, the function need to transverse row by row and column by column. So, the steps required for the function to run in order to retrieve all element is equal to the sqaure of the size. Therefore, the Big-O notation of this block of code is O(N^2) as the complexity of the block of code is increases quadratically.
---
3. You are a web developer working on a web application. Customer support approached you and escalated a problem that users are currently facing. The user visited the page at http://this-is-a-sample-site.com/shop/items and while the web page actually loads, the list of items for sale is stuck at loading. It seems to be related to Javascript. As a web developer, how do you diagnose this and the approach you would take to narrow down the problem?
    
    To diagnose and identify the causes of the problem, we can utilise the console of the browser as the problem is likely to be caused by Javascript. By utilising the debugger in console, we can add breakpoint to the line or block of codes that are potentially to be the cause of the problem especially on the block of code that responsible for listing the items. As item listing are commonly done using loop, every loop involved in listing the items should be more thorougly inspected.

    Besides, we can also write more resilence code by implementing exceptions handling for all the components. With exceptions handling, the component that not functioning properly would throw an exception, and it can be logged and inspected using console easily. Timeout also important as some components may iterating or waiting for a response from other connection indefinitely. So, by implementing timeout, the component that takes exceptional long period of time should be timed out and inspected.
---
4. You have learnt languages like C and C++ in university. It is said that languages like C and C++ is powerful, much more powerful than languages like Java, Python, Ruby, etc.. Is it fair to say that all softwares should be written in C and C++? Why and why not?
    
    No, in my opinion, not all softwares should be written C or C++. The reason is that each programming language is designed to have its own unique feature and convenience, and the reason there are so many programming language out there is because each of it serves their own purposes. C and C++ is undeniably powerful and also serve as the foundation for many programming language, but these two languages also have their limitations such as C do not support object-oriented programming natively and C++ have security issue and no build-in thread support. Therefore, other programming language like Java and Python comes in to fill the shortages of C or C++. Morever, C and C++ also does not support mobile development natively, therefore languages like Swift, Java or Kotlin are more suitable for mobile softwares.

    In short, I would say that selection of programming language for writing softwares is depend on their intended usage and platform, and also the programmer's preferences. If writing low-level softwares like those that interact with hardwares, C or C++ may be the better choice. However, it is more wise and convenient to use other high level programming language like Java or Python if the programmer is more familiar to it.